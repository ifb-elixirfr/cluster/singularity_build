"""CLI to run CI for singularity images."""
import click
import os
import os.path
import subprocess
import sys
import yamale

from git import Repo
from jinja2 import Template as JinjaTemplate
from string import Template
from yaml import load, Loader

CWD = os.path.abspath(os.getcwd())
DIST_PATH = os.path.join(CWD, 'dist')


@click.group()
def ci():
    """Create main group."""
    # check environment
    for variable in ('IMAGES_PATH', 'MODULEFILES_PATH', 'WRAPPERS_PATH', 'CLUSTER_IMAGES_PATH', 'CLUSTER_WRAPPERS_PATH'):
        if variable not in os.environ or len(os.environ[variable]) == 0:
            click.echo(message="{} is undefined".format(variable),
                       err=True)
            sys.exit(1)


@ci.command()
@click.option('--gitrange', nargs=2)
def get_modified_meta(gitrange):
    """Get modified meta file list."""
    (before, current) = gitrange
    repo = Repo('.')
    rev_before = repo.commit(before)
    diffs = rev_before.diff(current)

    for diff in diffs:
        if diff.b_path.endswith('.yml') and diff.b_path not in ('.gitlab-ci.yml', 'ci/meta_schema.yml'):
            software = os.path.dirname(diff.b_path)
            version = os.path.basename(diff.b_path)[:-4]
            print("{}/{}".format(software, version))


@ci.command()
@click.option('--gitrange', nargs=2)
def get_modified_def(gitrange):
    """Get modified definition file list."""
    (before, current) = gitrange
    repo = Repo('.')
    rev_before = repo.commit(before)
    diffs = rev_before.diff(current)

    for diff in diffs:
        if diff.b_path.endswith('.def'):
            software = os.path.dirname(diff.b_path)
            version = os.path.basename(diff.b_path)[:-4]
            print("{}/{}".format(software, version))


@ci.command()
@click.option('--software')
@click.option('--version')
def check_metadata(software, version):
    """Check meta data for the given software version"""
    schema_path = os.path.join('ci', 'meta_schema.yml')

    meta_path = os.path.join(software, "{}.yml".format(version))
    if not os.path.exists(meta_path):
        click.echo(
            message="No meta file for {software} {version} ({meta_path})".format(
                software=software,
                version=software,
                meta_path=meta_path),
            err=True)
        sys.exit(1)

    schema = yamale.make_schema(schema_path)
    data = yamale.make_data(meta_path)
    try:
        yamale.validate(schema, data)
    except ValueError as ve:
        click.echo(
            message="Invalid meta file for {software} {version} ({meta_path})\n{message}"
                    .format(
                        software=software,
                        version=version,
                        message=ve,
                        meta_path=meta_path),
            err=True)
        sys.exit(1)


@ci.command()
@click.option('--software')
@click.option('--version')
def build_image(software, version):
    """Build an image"""
    image_filename = get_image_filename(software, version)
    defintion_path = os.path.join(CWD, software, "{}.def".format(version))

    if not os.path.exists(defintion_path):
        click.echo("Nothing to do for %s-%s" % (software, version))
        sys.exit(0)

    os.chdir(os.path.dirname(defintion_path))

    image_path = os.path.join(os.environ['IMAGES_PATH'], image_filename)
    build_cmd = ['singularity', 'build', '--force', image_path, defintion_path]
    build = subprocess.run(build_cmd)

    os.chdir(CWD)

    if build.returncode != 0:
        click.echo(message="Failed to build %s %s" % (software, version),
                   err=True)
        sys.exit(1)


@ci.command()
@click.option('--software')
@click.option('--version')
def build_wrappers(software, version):
    """Build and deploy wrappers and modulefile."""
    meta_path = os.path.join(CWD, software, "{}.yml".format(version))

    if not os.path.exists(meta_path):
        click.echo("No wrappers to build for %s-%s" % (software, version))
        sys.exit(0)

    meta = load(open(meta_path, "r"), Loader=Loader)

    common_options = ''
    if 'with_gpu_support' in meta and meta['with_gpu_support'] is True:
        common_options += '--nv'

    binds = meta.get('binds', [])

    binaries = []
    binaries_paths = meta.get('binaries_paths', [])
    for binaries_path in binaries_paths:
        binaries += ls_executable_in_image(software, version, binaries_path)

    if 'binaries' in meta:
        binaries += meta['binaries']

    for binary in binaries:
        options = common_options
        if type(binary) == dict:
            name = list(binary.keys())[0]
            if 'binds' in binary[name]:
                binds = binds + binary[name]['binds']
        else:
            name = binary

        if len(binds) > 0:
            if len(options) > 0:
                options += ' '
            options += '-B '
            options += ','.join(
                ['{src}:{dest}'.format(**bind) for bind in binds])

        build_wrapper(software, version, name, options)

    if 'make_env_wrapper' in meta and meta['make_env_wrapper'] is True:
        build_wrapper(software, version, None, options)

    template_path = os.path.join(os.path.dirname(__file__), 'modulefile.j2')
    template_desc = open(template_path, 'r')
    modulefile_template = JinjaTemplate(template_desc.read())

    modulefiles_dir = os.path.join(os.environ['MODULEFILES_PATH'], software)
    os.makedirs(modulefiles_dir, exist_ok=True)
    modulefile_path = os.path.join(modulefiles_dir, version)
    wrappers_path = os.path.join(os.environ['CLUSTER_WRAPPERS_PATH'], software, version)
    description = meta['description']
    url = meta.get('url', '')
    modules = meta.get('require', [])

    modulefile = open(modulefile_path, "w")
    modulefile.write(modulefile_template.render(
        modules=modules,
        software=software,
        version=version,
        url=url,
        description=description,
        wrappers=wrappers_path))
    modulefile.close()


def get_image_filename(software, version):
    """Return default image path for software and version."""
    return "{software}-{version}.sif".format(software=software, version=version)


def build_wrapper(software, version, binary, options):
    """Buid and install a wrapper for a given binary."""
    wrapper_template = Template("""#! /usr/bin/env bash
singularity exec $options $image_path $binary $$@""")  #NOQA

    image_file = get_image_filename(software, version)

    wrappers_path = os.path.join(os.environ['WRAPPERS_PATH'], software, version)
    os.makedirs(wrappers_path, exist_ok=True)

    name = binary if binary is not None else 'run_with_{}'.format(software)

    wrapper_path = os.path.join(wrappers_path, name)
    wrapper = open(wrapper_path, "w")
    wrapper.write(wrapper_template.substitute(
        image_path=os.path.join(os.environ['CLUSTER_IMAGES_PATH'], image_file),
        binary=binary if binary is not None else '',
        options=options))
    wrapper.close()
    os.chmod(wrapper_path, 0o755)


def ls_executable_in_image(software, version, path):
    image_path = os.path.join(os.environ['IMAGES_PATH'], get_image_filename(software, version))

    ls = subprocess.run(['singularity', 'exec', image_path, 'find', path, '-maxdepth', '1', '-type', 'f', '-executable', '-print'],
                        stdout=subprocess.PIPE)
    result = ls.stdout.decode('utf-8')
    files = result.split('\n')
    files = [os.path.basename(file) for file in files[:-1]]
    return files


if __name__ == '__main__':
    ci()
